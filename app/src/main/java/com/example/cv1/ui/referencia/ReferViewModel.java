package com.example.cv1.ui.referencia;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ReferViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ReferViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("REFERENCIAS PERSONALES");
    }

    public LiveData<String> getText() {
        return mText;
    }
}