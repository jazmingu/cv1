package com.example.cv1.ui.recon;

import androidx.lifecycle.LiveData;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

public class ReconViewModel extends ViewModel {

    private MutableLiveData<String> mText;

    public ReconViewModel() {
        mText = new MutableLiveData<>();
        mText.setValue("SIN RESULTADOS");
    }

    public LiveData<String> getText() {
        return mText;
    }
}