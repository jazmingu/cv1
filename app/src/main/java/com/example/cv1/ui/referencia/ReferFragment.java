package com.example.cv1.ui.referencia;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;

import com.example.cv1.R;

public class ReferFragment extends Fragment {

    private ReferViewModel referViewModel;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        referViewModel =
                ViewModelProviders.of(this).get(ReferViewModel.class);
        View root = inflater.inflate(R.layout.fragment_refer, container, false);
        final TextView textView = root.findViewById(R.id.text_recon);
        referViewModel.getText().observe(getViewLifecycleOwner(), new Observer<String>() {
            @Override
            public void onChanged(@Nullable String s) {
                textView.setText(s);
            }
        });
        return root;
    }
}